Disabled:Let's forget this one: alpha and probably won't get any more revisions
Categories:Office
License:GPLv3
Web Site:https://code.google.com/p/dat255-doit
Source Code:https://code.google.com/p/dat255-doit/source
Issue Tracker:https://code.google.com/p/dat255-doit/issues

Auto Name:doIT
Summary:Todo lists
Description:
Create tasks and sort by due date or priority.
.

Repo Type:git-svn
Repo:https://dat255-doit.googlecode.com/svn/trunk

Build Version:1.0,1,110,subdir=doIT,target=android-10,prebuild=rm -rf ./doIT.apk

Auto Update Mode:None
Update Check Mode:Static
Current Version:1.0
Current Version Code:1

