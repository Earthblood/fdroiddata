Categories:System
License:GPLv3
Web Site:http://brandroid.org/open
#Don't link to fdroid branch as GitHub clients don't like it
Source Code:https://github.com/BrandroidTools/OpenExplorer
Issue Tracker:https://github.com/BrandroidTools/OpenExplorer/issues
Donate:http://brandroid.org/donate.php

Auto Name:Open Explorer Beta
Summary:File manager
Description:
Open Explorer – file manager for all devices! (Tablets, Phones, Google TVs,
sandwiches!)

* Complete file system management
* USB, SD and other external media
* Network connectable (FTP, SFTP, SMB/Samba/Windows/Lan/WIFI)
* Text Editor (Tabbed, Multiple Document Interface)
* Smart Folders that scan for different types of media (Videos, Photos, Music, Downloads)
* View Pager directory navigation (swipe right to go up a directory)
* Superuser/Root ability to list/read system folders (no system write yet)
* More features like offline file structure cache, more Networking capability and Disk space heat map to come.

The source code for our build is in a branch called fdroid.
.

Repo Type:git
Repo:https://github.com/BrandroidTools/OpenExplorer.git

#N.B use fdroid branch
Build Version:0.189,189,8e18bb5ee7,update=.;ActionBarSherlock/library,\
submodules=yes,init=rm -rf releases/
Build Version:0.190,190,e21839d09e,update=.;ActionBarSherlock/library,\
submodules=yes,init=rm -rf releases/
Build Version:0.193,193,333851a72,update=.;ActionBarSherlock/library,submodules=yes,prebuild=\
rm -rf releases/
Build Version:0.194,194,03ab52ea1,update=.;ActionBarSherlock/library,submodules=yes,prebuild=\
rm -rf releases/
#Submodules not working
Build Version:0.208,208,bec59bad08,\
init=rm -rf ActionBarSherlock && git clone https://github.com/BrandroidTools/ActionBarSherlock.git,\
update=.;ActionBarSherlock/library,prebuild=rm -rf releases/
Build Version:0.212,212,c7da3bbf29,\
init=rm -rf ActionBarSherlock && git clone https://github.com/BrandroidTools/ActionBarSherlock.git,\
update=.;ActionBarSherlock/library,prebuild=rm -rf releases/

Auto Update Mode:None
Update Check Mode:RepoManifest/fdroid
Current Version:0.212
Current Version Code:212

