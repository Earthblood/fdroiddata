Categories:Multimedia
License:GPLv2+
Web Site:https://code.google.com/p/daap-client
Source Code:https://code.google.com/p/daap-client/source
Issue Tracker:https://code.google.com/p/daap-client/issues

Auto Name:DAAP
Summary:DAAP (Music/media sharing) Client
Description:
Client for DAAP (Digital Audio Access Protocol) servers. Supports streaming,
queuing, playlists, and searching.
.

Repo Type:git-svn
Repo:https://daap-client.googlecode.com/svn/trunk

Build Version:.9.1,30,62
Build Version:.9.2,31,!Repo code is missing at least one file update - the manifest
Build Version:.9.6.2,38,!No corresponding source in repo
Build Version:.9.6.5,39,89
Build Version:.9.7,40,90
Build Version:.9.7+,45,107,forceversion=yes,target=android-8
# forgot to bump version ... again
Build Version:.9.7++,46,108,forceversion=yes,forcevercode=yes,target=android-8

Auto Update Mode:None
Update Check Mode:None
Current Version:.9.7
Current Version Code:46

