Categories:Phone & SMS
License:MIT
Web Site:
Source Code:https://github.com/dinosaurwithakatana/lastcaller
Issue Tracker:https://github.com/dinosaurwithakatana/lastcaller/issues

Name:Dashclock: Last Caller
Summary:Previous caller on lock screen
Description:
Extension for [[net.nurik.roman.dashclock]].
.

Repo Type:git
Repo:https://github.com/dinosaurwithakatana/lastcaller.git

Build:2.1,8
    commit=c0207d37c8ac9a1

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.1
Current Version Code:8

