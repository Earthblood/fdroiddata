Categories:Office
License:GPLv3+
Web Site:http://mirakel.azapps.de
Source Code:https://github.com/azapps/mirakel-dashclock
Issue Tracker:https://github.com/azapps/mirakel-dashclock/issues
Donate:http://mirakel.azapps.de/help_us.html#donate
FlattrID:2188714

Auto Name:Mirakel-Dashclock
Summary:Dashclock extension for Mirakel
Description:
Extension for [[net.nurik.roman.dashclock]] that integrates with
[[de.azapps.mirakelandroid]].
.

Repo Type:git
Repo:https://github.com/azapps/mirakel-dashclock.git

Build:1.1,4
    commit=v1.1

Build:1.2,5
    commit=v1.2

Build:1.3,7
    commit=42e2cd3f4

Build:1.4,8
    commit=v1.4

Build:1.5,9
    commit=v1.5

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.5
Current Version Code:9

