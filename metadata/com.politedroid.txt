Categories:Office
License:GPLv3
Web Site:https://github.com/miguelvps/PoliteDroid
Source Code:https://github.com/miguelvps/PoliteDroid
Issue Tracker:https://github.com/miguelvps/PoliteDroid/issues

Auto Name:Polite Droid
Summary:Calendar tool
Description:
Activates silent mode during calendar events.
.

Repo Type:git
Repo:https://github.com/miguelvps/PoliteDroid.git

Build Version:1.2,3,6a548e4b19,target=android-10
Build Version:1.3,4,ad865b57bf3ac59580f38485608a9b1dda4fa7dc,target=android-15
Build Version:1.4,5,456bd615f3fbe6dff06433928cf7ea20073601fb,target=android-10

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:5

