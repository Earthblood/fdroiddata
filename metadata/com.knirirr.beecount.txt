Categories:Office
License:Apache2
Web Site:https://code.google.com/p/beecount
Source Code:https://code.google.com/p/beecount/source
Issue Tracker:https://code.google.com/p/beecount/issues

Auto Name:BeeCount
Summary:Knitting row counter
Description:
A knitting project helper, with the following features:
* Keep track of several knitting projects.
* For each project, track several items which need counting, e.g. rows, pattern repeats etc
* Allow counting up, down and re-setting to zero of these counts.
* Allow editing of projects to add, remove or change counts after project creation.
* Link counts together so that one increments another.
* Set row alerts and cause counts to reset at particular values.
* Allow backing up and restoring of the entire projects database.
.

Repo Type:hg
Repo:https://code.google.com/p/beecount

Build:1.2.1,47
    commit=1.2.1
    target=android-15

Build:1.2.2,48
    commit=1.2.2
    target=android-15

Build:1.2.3,50
    commit=1.2.3
    target=android-15

Build:1.2.5,53
    commit=1.2.5
    target=android-15

Build:1.2.7,56
    commit=1.2.7
    target=android-15

Build:1.2.8,59
    disable=wrongly tagged, as is 1.2.9

Build:1.2.10,62
    commit=1.2.10
    target=android-15

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2.10
Current Version Code:62

