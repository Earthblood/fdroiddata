Categories:Games
License:Apache2
Web Site:http://www.onyxbits.de/textfiction
Source Code:https://github.com/onyxbits/TextFiction
Issue Tracker:https://github.com/onyxbits/TextFiction/issues

Auto Name:Text Fiction
Summary:Interactive fiction interpreter
Description:
ZPlet based Z-Machine interpreter for playing text-only adventures games.
The interface is like an SMS app
and there's a list of buttons for common actions as well as the ability
to select text from the page to be an action.

Supports games in Z3, Z5, Z8 format and games can be download in the app.
Savegames are kept on the sdcard.
.

Repo Type:git
Repo:https://github.com/onyxbits/TextFiction.git

Build:1.2,3
    commit=v1.2
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.0,4
    commit=v2.0
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.2,6
    commit=v2.2
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.2
Current Version Code:6

