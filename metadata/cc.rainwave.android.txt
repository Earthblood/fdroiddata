Categories:Multimedia
License:NewBSD
Web Site:http://rainwave.cc
Source Code:https://github.com/OEP/rainwave-android
Issue Tracker:https://github.com/OEP/rainwave-android/issues

Auto Name:Rainwave
Summary:Client for music station
Description:
Listen to music on rainwave.cc, an icecast music station, via music players
like [[com.android.music]] or [[net.sourceforge.servestream]]. You can also
log in to vote and request tracks to be played.
.

Repo Type:git
Repo:https://github.com/OEP/rainwave-android.git

Build:1.1,4
    commit=36477326ec80e
    init=sed -i 's/minSdkVersion="3"/minSdkVersion="4"/g' AndroidManifest.xml
    target=android-8

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.2beta3
Current Version Code:8

