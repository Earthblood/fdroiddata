Categories:Science & Education
License:GPLv3
Web Site:https://code.google.com/p/fakedawn
Source Code:https://code.google.com/p/fakedawn/source
Issue Tracker:https://code.google.com/p/fakedawn/issues

Auto Name:Fake Dawn
Summary:Gentle alarm clock
Description:
Fake Dawn is an alarm clock that gradually increases brightness and sound volume to lead you out of
deep sleep and wake you up gently. You can adjust when the brightness of the screen starts to rise
and reaches its maximum; when the sound starts to play and how the volume increases.

Features:
* fully customize the behavior of the dawn alarm
* choose the screen color from a wide range
* choose custom sound alarm or ring tone
* vibrate with sound
.

Repo Type:git
Repo:https://code.google.com/p/fakedawn

Build Version:1.1,2,v1.1
Build Version:1.2,3,v1.2

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2
Current Version Code:3

