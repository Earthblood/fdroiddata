Categories:Multimedia
License:GPLv3
Web Site:https://github.com/adrian-bl/vanilla
Source Code:https://github.com/adrian-bl/vanilla
Issue Tracker:https://github.com/adrian-bl/vanilla/issues

Auto Name:Vanilla Music
Summary:Music player
Description:
Music player with a simple interface similar to the stock music player.
Supports browsing by folder, queueing, shake-to-skip and more. Supports
the same audio formats as the stock music player since the system media
APIs are used.

This is a fork of [[org.kreed.vanilla]] which is no longer being developed
by its author. We keep that in the repo as it supports Android versions below 4.0.3.
.

Repo Type:git
Repo:https://github.com/adrian-bl/vanilla.git

Build Version:0.9.12,912,669bf3f4b8fe8bc
Build Version:0.9.13,913,0.9.13
Build Version:0.9.14,914,0.9.14
Build Version:0.9.15,915,0.9.15
Build Version:0.9.17,917,0.9.17
Build Version:0.9.18,918,0.9.18
Build Version:0.9.20,920,0.9.20
Build Version:0.9.21,921,0.9.21
Build Version:0.9.22,922,0.9.22
Build Version:0.9.23,923,0.9.23

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.9.23
Current Version Code:923

