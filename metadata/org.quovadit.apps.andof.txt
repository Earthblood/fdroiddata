Categories:Multimedia
License:GPLv3
Web Site:http://andof.quovadit.org
Source Code:http://sourceforge.net/p/andof/code
Issue Tracker:http://sourceforge.net/p/andof/tickets

Auto Name:anDOF
Summary:Calculate DOF for photography
Description:
Simple app to calculate depth of field
.

Repo Type:git
Repo:git://git.code.sf.net/p/andof/code

Build:1.3,9
    commit=v1.3
    target=android-17

Build:1.4,10
    commit=v1.4
    target=android-18

Build:1.5,14
    commit=v1.5

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.5
Current Version Code:14

