Categories:System
License:Apache2
Source Code:https://android.googlesource.com/platform/packages/inputmethods/PinyinIME
Issue Tracker:

Auto Name:谷歌拼音输入法
Summary:Input method for Pinyin
Description:
Pinyin IME from the AOSP. It is not only of use to Chinese writers but to English writers too because
it has a T9 style keyboard for writing in English.
.

Repo Type:git
Repo:https://android.googlesource.com/platform/packages/inputmethods/PinyinIME

Build:4.2.1,1
    commit=android-4.4.2_r1
    disable=uses import android.os.SystemProperties
    target=android-19
    patch=build_1.patch;ndk-build_1.patch
    prebuild=mv lib/com/android/inputmethod/pinyin/* src/com/android/inputmethod/pinyin/ && \
         echo -e 'version.name=4.2.1\nversion.code=1' > ant.properties
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Static

