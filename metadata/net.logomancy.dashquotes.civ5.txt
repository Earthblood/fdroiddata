Categories:Wallpaper
License:GPLv3
Source Code:https://github.com/logomancer/dashquotes-civ5
Issue Tracker:https://github.com/logomancer/dashquotes-civ5/issues

Name:DashClock: Civilization V Quotes
Summary:Random quotes on lock screen
Description:
.

Repo Type:git
Repo:https://github.com/logomancer/dashquotes-civ5.git

Build Version:1.0,1,d700ca6a87,\
rm=libs/dashclock-api-r2.0.jar,\
srclibs=DashClock@ecb5a191880,\
prebuild=\
  echo 'source.dir=src;$$DashClock$$/api/src/main/java;\
    $$DashClock$$/api/src/main/aidl' >> project.properties

Auto Update Mode:None
Update Check Mode:None
Current Version:1.0
Current Version Code:1

