Categories:Navigation
License:GPLv2
Web Site:
Source Code:https://sourceforge.net/p/libwlocate/code
Issue Tracker:https://sourceforge.net/p/libwlocate

Name:OpenWLANMap
Auto Name:OpenWLANMap@Android
Summary:Help create a WiFi coverage map
Description:
For use in location services when no GPS is available (similar like Google
does but free and open and with an option to unsubscribe).
OpenWLANMap@Android is an App that helps to collect WLANs and their positions
and where user take part at a tracking contest.
.

Repo Type:srclib
Repo:libwlocate

Build:0.95,95
    commit=ebc753
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.01,101
    commit=0675a1
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.02,102
    commit=c0ea12
    subdir=master/android/OWMapAtAndroid
    target=android-19
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.03,103
    commit=799b7d
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.04,104
    commit=580567
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.07,107
    commit=8d621f2
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Build:1.08,108
    commit=0bd0fd4
    subdir=master/android/OWMapAtAndroid
    prebuild=echo 'source.dir=src;../LocDemo/src/com/vwp/libwlocate' > ant.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.09
Current Version Code:109

