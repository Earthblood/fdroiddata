Categories:System
License:GPLv3
Web Site:https://github.com/ligi/FAST
Source Code:https://github.com/ligi/FAST
Issue Tracker:https://github.com/ligi/FAST/issues

Auto Name:FAST App Search Tool
Summary:Find apps just by typing
Description:
Find your apps without needing to scroll through lists. It can display icons or
not and search for
package names too if the option is selected. Long-pressing an entry can display
options to view the App
Details, which is helpful for uninstalling apps, or to view the
app on Google Play.
Works as a normal app or a launcher.
.

Repo Type:git
Repo:https://github.com/ligi/FAST.git

Build:1.8,18
    commit=2436e89b3
    target=android-15
    srclibs=ActionBarSherlock@6e3f2bb5
    prebuild=echo "android.library.reference.1=$$ActionBarSherlock$$" >> project.properties

Build:1.9,19
    disable=no source code
    commit=unknown - see disabled

Build:2.9,29
    commit=2.9
    init=rm libs/android-support-v13.jar
    target=android-17
    extlibs=android/android-support-v13.jar
    prebuild=rm -rf promo

Build:3.3,33
    commit=3.3
    init=rm libs/android-support-v13.jar
    target=android-17
    extlibs=android/android-support-v13.jar
    prebuild=rm -rf promo

Build:3.7,37
    commit=3.7
    init=rm libs/android-support-v13.jar
    target=android-17
    extlibs=android/android-support-v13.jar
    prebuild=rm -rf promo

Build:4.2,42
    commit=4.2
    subdir=app
    gradle=forFDroid

Build:4.3,43
    commit=4.3
    subdir=app
    gradle=forFDroid

Build:4.9,49
    commit=4.9
    subdir=android
    gradle=forFDroid

Build:5.1,51
    commit=5.1
    subdir=android
    gradle=forFDroid

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:5.1
Current Version Code:51

